# Dan's Wherewego Website

Pretty much it's a mess, so sorry in advance for the confusion

## General structure

The general structure is something like this:

homepage is reset.php -> index.php -> event.php -> back to index.php upon hitting similar or different buttons

### header.php
* imports bootstrap, google api, stylesheet

### header2.php
* Logo -> goes to index.php
* Selection by city, city district, day -> set's session variables then back to index.php
* "submit" -> goes to submit.php
* "Reset" -> goes to reset.php
* "Liked Events" -> goes to liked.php

### footer.php
* About page
* Contact Page
* Terms of Service
* Privacy Policy
* Donate Page

## Individual Page Descriptions

### Intro.php
* Finds the users location, assigns them into a city district using location.js
* This is where I'm having problems cookies/session variables don't always set requiring me to reload the page multiple times in order for it to work

### Landpage.php
* Depending on variables set before find a new event
* First update the database depending on more/different
* choose new event using chooseeventdatelimit.php
* then clean the event name for usage in url
* Go to event page

### Eventpage.php
* Display the event information from the database, based on event ID number
* use lat/longitude cookies to draw directions map

I know this is kinda quick and dirty, but it's not too too complicated. All the recommendation
algorithm junk is in chooseeventdatelimit.php but you shouldn't have to look at that, but you
can if you're curious.

Thanks a bunch!

You can see the site as is at www.where-wego.com/alpha/reset.php
