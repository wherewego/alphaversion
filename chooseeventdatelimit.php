<?php
//error_reporting(E_ALL); ini_set('display_errors', 'On'); 
//session_start();
include_once('db.php');

if ($_SESSION["event"] == "new") {

    //New Session;
    //Find largest user id number
    $query1 = "SELECT MAX(userid) FROM ustable";
    $result1 = mysqli_query($connection, $query1);
    $usrow = mysqli_fetch_row($result1);

    //Add 1 to id number for new entry
    $newuserid = $usrow[0]+1 ;
    $_SESSION["s_userid"] = $newuserid ;

    //Add new user with all 0's to ustable
    $query1 = "INSERT INTO ustable (userid) VALUES ('$newuserid')";
    $result1 = mysqli_query($connection, $query1);
    if (!$result1) {
        echo "Couldn't do query" .mysqli_error($connection);
    }

    //Pull a random new event
    $evtid = ranevent($_SESSION["s_userid"],$connection);
    $_SESSION["evt_id"] = $evtid ;

} else {

    //Get Previous event number
    $prevnt = $_SESSION["evt_id"];

    //Get choice made on previous event
    //$choice = $_SESSION["choice"];

    //Find new non-seen event
    $evtid = recevent($_SESSION["s_userid"],$prevnt,$choice,$connection);
    $_SESSION["evt_id"] = $evtid ;

}

//find events within date selection criteria
function dateselect($connection,$criteria,$district) {

    // today: date_1<=today and date_2>=today
    // tomorrow: date_1<=tomorrow and date_2>=tomorrow
    // this week:  date_1<=d+7 and date_2>=d

    $today = date("Y-m-d");
    $tomorrow = date("Y-m-d",mktime(0, 0, 0, date("m")  , date("d")+1, date("Y")));
    $dplusseven = date("Y-m-d",mktime(0, 0, 0, date("m")  , date("d")+7, date("Y")));
    //$date = date("Y-m-d",mktime(0, 0, 0, date("m")  , date("d")+14, date("Y")));

    if ($criteria == 'Today') {
        $query4 = "SELECT eventid, district FROM evtable WHERE date_1<='". $today . "' AND date_2>='". $today ."'";
    } elseif ($criteria == 'Tomorrow') {
        $query4 = "SELECT eventid, district FROM evtable WHERE date_1<='". $tomorrow . "' AND date_2>='". $tomorrow ."'";
    } else {
        $query4 = "SELECT eventid, district FROM evtable WHERE date_1<='". $dplusseven . "' AND date_2>='". $today ."'";
    }
    //Change this:
    $result4 = mysqli_query($connection, $query4);

    $cnt=1;
    $datearray=[] ;

    //Find events within certain dates
    while($row = mysqli_fetch_row($result4)) {
        $datearray[$cnt] = $row[0] ;
        $districtarray[$cnt] = $row[1] ;
        $cnt=$cnt+1;
    }
    mysqli_free_result($result4);
    return array($datearray,$districtarray);
}

//-----------------------------------------------------------------------------------------



//pull a new random event (event=0) for new user-------------------------------------------

function ranevent($usid,$connection) {

    //Find events within dates selected
    $selected = dateselect($connection,$_SESSION["dates"],$_COOKIE['districtcookie']);

    $datearray = $selected[0] ;
    $numevents = count($datearray);

    //generate random number from 1 to count
    $evtid = $datearray[rand(1,$numevents)];

    //echo 'Random Event Id ' . $evtid ;

    return $evtid;
}

//------------------------------------------------------------------------------------------------------

function recevent($usid,$r,$choice,$connection) {

    $userdistrict = $_COOKIE['districtcookie'];
    //$userdistrict = $districttest  ;
    $eventlimit=100;
    $userlimit =50;

    //Pick an event
    $selected = dateselect($connection,$_SESSION["dates"], $_COOKIE['districtcookie']);

    $datearray = $selected[0] ;
    $districtarray = $selected[1];

    $numevents = count($datearray);

    //Number of Events
    $maxev = $numevents ;

    //Number of users
    $maxus = $usid ;

    //current user: usid
    //last event: r

    //Fill matrix------------------------------------------------------------------------
    //Need random event selection
    $events = range(1,count($datearray));
    shuffle($events);

    //Use event selection to select right events
    $eventstring='';

    for ($i = 1; $i <= count($datearray); $i++) {
        if ($i<$eventlimit) {
            $eventstring = $eventstring . 'ev' . $datearray[$events[$i-1]] .',';
        }
        if ($datearray[$events[$i-1]]==$r) {
            $eventstring = $eventstring . 'ev' . $datearray[$events[$i-1]] .',';
            if ($i<$eventlimit) {
                $lastev = $i-1 ;
            } else {
                $lastev = $eventlimit-1;
            }
        }

    }
    $eventtrim = rtrim($eventstring, ",");
    //echo $eventtrim ;

    $r = $lastev ;

    // Bring in user data
    $query1 = "SELECT ". $eventtrim ." FROM ustable";
    $result1 = mysqli_query($connection, $query1);

    $test = [] ;

    $a=0;
    while($row = mysqli_fetch_row($result1)) {
        $max = sizeof($row);
        for($b=0; $b<$max; $b++) {
            $test[$a][$b] = $row[$b];
        }
        $a++ ;
    }

    // Find empty values ----------------------------------------------------------------

    $evtidary[1] = 0;
    $cnt=1;
    $row1 = $test[$usid-1] ;

    //echo "<br />";
    //print_r($row1);
    //echo "<br />";

    for ($j = 0; $j<count($row1); $j++) {
        if ($row1[$j]==0) {
            $evtidary[$cnt]=$j+1;
            $cnt++ ;
        }
    }

    if ($evtidary[1]==0) {
        //echo 'No more events';
        //echo "<br />";
        return 0 ;
        exit();
    }

    //Compute similarity magnitude-----------------------------------------------------------------

    $evsize = sizeof($evtidary);
    $res = array_fill(1,$evsize,0);

    for($i=1;$i<=$evsize-1;$i++) {

        $top=0;
        $mag1=1;
        $mag2=1;
        $g = $evtidary[$i]-1;
        $usrcnt = min($maxus-1,$userlimit-1);
        //$users = range(1,$maxus-1);
        //shuffle($users);

        for($p=0; $p<$usrcnt; $p++) {
            //$k = $users[$p];
            $x = $test[$p][$g] ;
            $y = $test[$p][$r] ;
             $top = $top + ($x * $y);
            $mag1 = $mag1 + ($x * $x);
            $mag2 = $mag2 + ($y * $y);
        }

        //echo "<br \>";
        //print_r($evtidary);
        //echo "<br \>";
        //print_r($events);
        //echo "<br \>";
        //print_r($districtarray);
        //echo "<br \>";

        $evdistrict = $districtarray[$events[$evtidary[$i]-1]] ;
        $evname = $datearray[$events[$evtidary[$i]-1]] ;


        //Choose Multiplier depending on district
        if ($evdistrict==$userdistrict) {
            $multiplier=20 ;
        } else {
            switch ($userdistrict) {
                case "A":
                    if ($evdistrict=="D"||"B"||"E") {$multiplier=5;}
                    if ($evdistrict=="C") {$multiplier=2;}
                    if ($evdistrict=="F") {$multiplier=-1;}
                    break;
                case "B":
                    if ($evdistrict=="A"||"C") {$multiplier=5;}
                    if ($evdistrict=="D"||"E") {$multiplier=2;}
                    if ($evdistrict=="F") {$multiplier=-1;}
                    break;
                case "C":
                    if ($evdistrict=="B"||"A") {$multiplier=5;}
                    if ($evdistrict=="D"||"E") {$multiplier=2;}
                    if ($evdistrict=="F") {$multiplier=-1;}
                    break;
                case "D":
                    if ($evdistrict=="C"||"B"||"A") {$multiplier=5;}
                    if ($evdistrict=="E"||"F") {$multiplier=-1;}
                    break;
                case "E":
                    if ($evdistrict=="C"||"B"||"A") {$multiplier=5;}
                    if ($evdistrict=="D"||"F") {$multiplier=-1;}
                    break;
                case "F":
                    if ($evdistrict=="C"||"D"||"E"||"F") {$multiplier=5;}
                    if ($evdistrict=="B"||"A") {$multiplier=-1;}
                    break;
                default:
                   $multiplier=1;
            }
        }

        //$message = "User District:".$userdistrict." Event District: ". $evdistrict . " Event ID: ".$evname. " Multiplier:".$multiplier."<br \>";
        //echo $message;

        // Compute dot product across table events-------------------------------------------
        $res[$i] = $multiplier * $top / (sqrt($mag1)*sqrt($mag2));
    }

    //Alter for different choices

    if ($choice==1) {
        $maxs = array_keys($res, max($res));
        //print_r($res);
        $num2 = $maxs[0] ;
    } else {
        $maxs = array_keys($res, min($res));
        //print_r($res);
        $num2 = $maxs[0] ;
    }

    $evtid =  $datearray[$events[$evtidary[$num2]-1]]   ;
    mysqli_free_result($result1);
    //echo 'Recommended Event Id ' . $evtid ;
    return $evtid;
}
?>
