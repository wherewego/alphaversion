<?php
session_start();
$pageTitle = 'About';
include_once('utils.php');
include('header.php');
include('header2.php');
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-1 col-sm-2 col-md-3"></div>
        <div class="col-xs-10 col-sm-8 col-md-6" id="about">

            <div id='abouth'>
            <h2>About Wherewego</h2>
            <h3>Getting you where you want to be</h3>
            </div>

            <p>The aim of the Wherewego website and mobile app is to provide an easy to use comprehensive events listing service targeted towards the individual user. The site uses previous event feedback to recommend the best event in the area for the individual user. This will be accomplished by scouring the internet for all the events in the user’s area, and then use the cumulative feedback from all previous users to arrive at a series of personalized event recommendations.
            </p>

            <p>Features will include customizable date selection, city district selection, and mode of transportation selection. For each event the basic information of the event as well as directions to the event from your current location will be provided. Revenue will be created from advertising from event promotors and event related products. Having promoted events also has the potential to be a revenue stream.
            </p>

            <p>The site intends to initially be a web service only, and then developed into a mobile app for android and apple products at later date. Wherewego will also be a company committed to providing livable wages for all its employees and strive to be a model of co-operative success to other companies.
            </p>

            <p>Wherewego operates only in Toronto, but intends to expand to other Canadian cities as soon as it is possible. There currently is no usable comprehensive event aggregator/recommender and as such Wherewego intends to capitalize on this market opportunity to the best of our ability. Wherewego was founded in September 2015 by Chief Executive Officer and President Dan Nichol and launched in Beta in 2016.
            </p>

            <p> If you'd like to contact us send us an email at <a href="mailto:contact@where-wego.com">contact@where-wego.com</a></p>

        </div>
        <div class="col-xs-1 col-sm-2 col-md-3"></div>
    </div>
</div>
<?php include('footer.php');?>