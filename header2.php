<nav class="navbar navbar-default navbar-fixed-top" id='headerh'>
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarFull">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="index.php"><img src="images/optimized/newlogo9.png" alt="Home" style="width:137px;height:50px;border:0;"></a>
        </div>
        <div class="collapse navbar-collapse" id="navbarFull">
            <ul class="nav navbar-nav">
                <li class="dropdown" id="date">
                    <a class="dropdown-toggle" data-toggle="dropdown" href=""> Toronto <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">Toronto</a></li>
                    </ul>
                </li>
                <li class="dropdown" id="location">
                    <a class="dropdown-toggle" data-toggle="dropdown" href=""> <?php echo $_SESSION["districtname"] ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="downtown.php">Downtown</a></li>
                        <li><a href="midtown.php">Midtown</a></li>
                        <li><a href="northyork.php">North York</a></li>
                        <li><a href="torwest.php">Toronto West</a></li>
                        <li><a href="toreast.php">Toronto East</a></li>
                    </ul>
                </li>
                <li class="dropdown" id="date">
                    <a class="dropdown-toggle" data-toggle="dropdown" href=""> <?php echo $_SESSION["dates"] ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="dates/today.php">Today</a></li>
                        <li><a href="dates/tomorrow.php">Tomorrow</a></li>
                        <li><a href="dates/thisweek.php">This Week</a></li>
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="liked.php">Liked</a></li>
                <li><a href="reset.php">Reset</a></li>
                <li><a href="submit.php">Submit</a></li>

                <li><a href="https://twitter.com/WhereWeGoEvents" target="_blank"><img src="images/stock/twicon.png" alt="Twitter" style="width:20px;height:20px;border:0;"></a></li>

                <li><a href="https://www.facebook.com/Wherewego-1544885942470114" target="_blank"><img src="images/stock/fbicon.png" alt="Facebook" target="_blank" style="width:20px;height:20px;border:0;"></a></li>
            </ul>                         

        </div>
    </div>
</nav>