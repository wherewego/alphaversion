<?php
//error_reporting(E_ALL); ini_set('display_errors', 'On'); 
session_start();

include_once('db.php');
include_once('utils.php');

if (isset($_GET['id'])){
    $evtid = $_GET['id'];    
} else {
    $evtid = 1 ; 
}

if (isset($_GET['name'])){
   $evname = $_GET['name'];    
} 


if ($evtid!=0) {
    //Fetch Event data-------------------------------------------------------------------
    $query3 = " SELECT * FROM evtable WHERE eventid=$evtid LIMIT 1 ";
    $result3 = mysqli_query($connection, $query3);
    if (!$result3) {echo "Couldn't do query" .mysqli_error($connection);}
    $eventrow = mysqli_fetch_row($result3);
    $endloc = $eventrow[5] ;
    mysqli_free_result($result3);

} else {    
   $endloc = "Toronto, On";
   $eventrow = array("", "No More Events Found", "", "", "", "", "", "", "", "", "", "");
}

setlocale(LC_ALL, 'en_US.UTF8');
function toAscii($str, $replace=array(), $delimiter='-') {
    if ( !empty($replace) ) {
        $str = str_replace((array)$replace, ' ', $str);
    }

    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

    return $clean;
}


function toDate($str) {
    $pieces = explode("-", $str);
    $year=$pieces[0];
    $month1=$pieces[1];
    $day=$pieces[2];

    switch ($month1) {
        case '01':
            $month='Jan.'; 
            break;
        case '02':
            $month='Feb.'; 
            break;
        case '03':
            $month='March'; 
            break;
        case '04':
            $month='April'; 
            break;
        case '05':
            $month='May'; 
            break;
        case '06':
            $month='June'; 
            break;
        case '07':
            $month='July'; 
            break;
        case '08':
            $month='Aug.'; 
            break;
        case '09':
            $month='Sept.'; 
            break;
        case '10':
            $month='Oct.'; 
            break;
        case '11':
            $month='Nov.'; 
            break;
        case '12':
            $month='Dec.'; 
            break;
        default:
            $month=''; 
    }

    $clean = $month.' '.ltrim($day, '0') ; 

    return $clean;
}

$ntest = toAscii($eventrow[1]);

if ($evname!=$ntest){

    //If name not found, check if future iteration of name exists, TODO, If it exists goto future event

    header('Location: 404.php');
}


if (strlen($eventrow[4])>0) {
    $at1 = ' at  ';
} else {
    $at1 ='';
}

$at2='';
if (strlen($eventrow[11])>1) {
    $at2 = ' at ';
}
if (strlen($eventrow[11])>8) {
    $at2 =' from ';
}

$titev = ' | event' ; 

$pageTitle = $eventrow[1].$titev ;
include('header.php');
include('header2.php');
?>

<div id="swipe-popup">
    <h3>Swipe</h3>
    <span id="swipe-icon" class="icon-one-finger-swipe-horizontally"></span>
    <div id="swipe-desc">&#8592; Different Similar &#8594;</div>
</div>

<div class="container-fluid">

    <div class="row">
        <div class="col-xs-1 col-sm-2 col-md-3"></div>
        <div class="col-xs-10 col-sm-8 col-md-6" id="thing"><?php echo $eventrow[1]?></div>
        <div class="col-xs-1 col-sm-2 col-md-3"></div>
    </div>
    <div class="row">
        <div class="col-xs-1 col-sm-2 col-md-3"></div>
        <div class="col-xs-10 col-sm-8 col-md-6" id="place"><span id="at"><?php echo $at1; ?></span><?php echo $eventrow[4]; ?><span id="at"><?php echo $at2; ?></span><?php echo $eventrow[11]; ?></div>
        <div class="col-xs-1 col-sm-2 col-md-3"></div>
    </div>
    <div class="row">
        <div class="col-xs-0 col-sm-2 col-md-4"></div>
        <div class="col-xs-6 col-sm-4 col-md-2" id="nope"><a href="index.php?choice=0" class="btn btn-default" id="different" role="button">Different</a></div>
        <div class="col-xs-6 col-sm-4 col-md-2" id="yep"><a href="index.php?choice=1" class="btn btn-default" id="similar" role="button">Similar</a></div>
        <div class="col-xs-0 col-sm-2 col-md-4"></div>
    </div>

    <div class="row" >
        <div class="col-xs-0 col-sm-1 col-md-3 "></div>

        <div class="col-xs-6 col-sm-5 col-md-3 " id="lgraybar">
            
            <p>
                <img src="images/optimized/calendar-min.png" alt="Calendar" style="width:20px;height:20px;border:0;">
                <?php 
                    if ($eventrow[2]==$eventrow[3]){
                        echo toDate($eventrow[2]) ; 
                    }else{
                        echo toDate($eventrow[2]).' to '.toDate($eventrow[3]) ; 
                    }    

                    if ($eventrow[3]<date("Y-m-d")){
                        echo " <span id='compl'>  Completed </span>  " ; 
                    }      
                ?>                             
            </p>                     
        </div>

        <div class="col-xs-6 col-sm-5 col-md-3 " id="rgraybar">
            <p><?php 
            if (strlen($eventrow[12])>1){
                echo $eventrow[12];
            }
            ?></p>
        </div>
             
        <div class="col-xs-0 col-sm-1 col-md-3" ></div>
    </div>

    <div class="row">
        <div class="col-xs-0 col-sm-1 col-md-3 "></div>

        <div class="col-xs-6 col-sm-5 col-md-3 " id="lgraybar2">
            <p>
            <a href="<?php echo $eventrow[8]?>" target="_blank"><img src="images/optimized/infobutton-min.jpg" alt="More Info" style="width:20px;height:20px;border:0;"></a>
            <a href="<?php echo $eventrow[8]?>" id="site" target="_blank" >More Info</a>
            </p>
        </div>

        <div class="col-xs-6 col-sm-5 col-md-3 " id="rgraybar2">
            <p id='travel_data'></p>            
        </div>       

        <div class="col-xs-0 col-sm-1 col-md-3 "></div>
    </div>


    <div class="row">
        <div class="col-xs-0 col-sm-1 col-md-3 "></div>
        <div class="col-xs-6 col-sm-5 col-md-3 " id="lgraybar3">
            <p>
            <a href="<?php echo $eventrow[9]?>" target="_blank"><img src="images/optimized/ticketbutton-min.jpg" alt="Tickets" style="width:21px;height:21px;border:0;"></a>
            <a href="<?php echo $eventrow[9]?>" id="tickets" target="_blank" >Tickets</a>
            </p>
        </div>   

        <div class="col-xs-6 col-sm-5 col-md-3 " id="rgraybar3">
            <p>
            <select id="mode" style="border:1">
                <option value="TRANSIT">Transit</option>
                <option value="DRIVING">Driving</option>
                <option value="WALKING">Walking</option>
                <option vlaue="BICYCLING">Bicycling</option>
            </select>
            </p>
        </div>

        <div class="col-xs-0 col-sm-1 col-md-3 "></div>
    </div>

    <div class="row">
        <div class="col-xs-0 col-sm-1 col-md-3"></div>
        <div class="col-xs-12 col-sm-10 col-md-6" id="map"></div>
        <div class="col-xs-0 col-sm-1 col-md-3"></div>
    </div>

    <!-- AddToAny BEGIN -->

    <div class="row">
        <div class="col-xs-0 col-sm-1 col-md-3"></div>
        <div class="col-xs-12 col-sm-10 col-md-6" id='shareup'>

            <div id='follow'>
                <a href="https://twitter.com/WherewegoEvents" class="twitter-follow-button" data-show-count="false">Follow @WherewegoEvents</a>
                <script>
                    !function(d,s,id){
                        var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){
                            js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);
                        }
                    }(document, 'script', 'twitter-wjs');
                </script>
            </div>

            <div id='share'>
                <div class="a2a_kit a2a_kit_size_32 a2a_default_style" >
                    <a class="a2a_dd" href="https://www.addtoany.com/share?linkurl=www.where-wego.com&amp;linkname="></a>
                    <a class="a2a_button_twitter"></a>
                    <a class="a2a_button_facebook"></a>                 
                </div>
            </div>
        </div>
        <div class="col-xs-0 col-sm-1 col-md-3"></div>
    </div>

    <script>
    var a2a_config = a2a_config || {};
    a2a_config.num_services = 6;
    </script>
    <script async src="https://static.addtoany.com/menu/page.js"></script>
    <!-- AddToAny END -->

</div>

<script>
endloc = <?php echo json_encode($endloc); ?>;
</script>

<script src="javascripts/location.js"></script>
<script src="javascripts/event.js"></script>
<script src="javascripts/swipe.js"></script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC6tAy9VIe7160odXZ2vqay4hyCXucFEd8&signed_in=true&callback=initMap">

//src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMRPMOnyxGaJq-3ypw2CeaQ-DGQx4X3ic&callback=initMap">
</script>

<?php include('footer.php'); ?>