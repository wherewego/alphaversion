<?php
//error_reporting(E_ALL); ini_set('display_errors', 'On'); 
session_start();
// Destroy all cookies.
foreach ( $_COOKIE as $key => $value ){
    if($key != 'PHPSESSID'){
        setcookie( $key, "", time()-1800);
    }
}
session_destroy();
session_start();
$_SESSION["event"] = "new";
$_SESSION["dates"] = "Today";
$pageTitle = "Homepage";
header('Location: index.php');
?>