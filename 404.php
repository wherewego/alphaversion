<?php
session_start();
include_once('db.php');
include_once('utils.php');
$pageTitle = "404 Page Not Found";
include('header.php');
include('header2.php');
?>

<div class="container-fluid">

    <div class="row">
        <div class="col-xs-1 col-sm-2 col-md-3"></div>
        <div class="col-xs-10 col-sm-8 col-md-6" id="notfound">
        	Event or Page Not Found
        	</div>
        <div class="col-xs-1 col-sm-2 col-md-3"></div>
    </div>

    <div class="row">
        <div class="col-xs-0 col-sm-2 col-md-4"></div>
        <div class="col-xs-12 col-sm-8 col-md-4" id="next"><a href="index.php" class="btn btn-default" id="similar" role="button">New Event</a></div>
        <div class="col-xs-0 col-sm-2 col-md-4"></div>
    </div>
    
</div>

<?php include('footer.php'); ?>