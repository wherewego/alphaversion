<?php
session_start();
$pageTitle = 'Contact';
include_once('utils.php');
include('header.php');
include('header2.php');
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-1 col-sm-2 col-md-3"></div>
        <div class="col-xs-10 col-sm-8 col-md-6" id="contact">
            <h2>Contact Us</h2>
            <p>
                If you have any questions or comments about <br />
                -    Site usability and feedback            <br />
                -    Event submission questions             <br />
                -    Event promotion and advertising        <br />
                -    Investment opportunities               <br />
                -    User data collection                   <br />
                Contact us via email here:                  <br />
                <a href="mailto:contact@where-wego.com">contact@where-wego.com</a>
            </p>
        </div>
        <div class="col-xs-1 col-sm-2 col-md-3"></div>
    </div>
</div>

<?php include('footer.php');?>