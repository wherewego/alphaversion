<?php
session_start();
include_once('db.php');

$name = mysqli_real_escape_string($connection, $_POST["q3_input3"]);
$location = mysqli_real_escape_string($connection, $_POST["q6_input6"]);

$date1 = $_POST["q4_input4"]['day'].'-'.$_POST["q4_input4"]['month'].'-'.$_POST["q4_input4"]['year'] ;
$date1 = mysqli_real_escape_string($connection, $date1);

echo $date1;

$date2 = $_POST["q5_input5"]['day'].'-'.$_POST["q5_input5"]['month'].'-'.$_POST["q5_input5"]['year']  ;
$date2 = mysqli_real_escape_string($connection, $date2);

$address = $_POST["q7_input7"]['addr_line1'].' '.$_POST["q7_input7"]['addr_line2'].', '.$_POST["q7_input7"]['city'].', ';
$address = $address.$_POST["q7_input7"]['state'].', '.$_POST["q7_input7"]['postal'].', '.$_POST["q7_input7"]['country'] ;
$address = mysqli_real_escape_string($connection, $address);

$starttime = $_POST["q8_startTime"]['hourSelect'].':'.$_POST["q8_startTime"]['minuteSelect'].' '.$_POST["q8_startTime"]['ampm'] ;
$starttime = mysqli_real_escape_string($connection, $starttime);

$genre = mysqli_real_escape_string($connection, $_POST["q14_eventGenre"]);
$description = mysqli_real_escape_string($connection, $_POST["q10_eventDescription"]);
$website = mysqli_real_escape_string($connection, $_POST["q11_eventWebsite11"]);
$tickets = mysqli_real_escape_string($connection, $_POST["q12_eventTickets"]);
$email = mysqli_real_escape_string($connection, $_POST["q13_contactEmail"]);

$district = 'A';
$free = 'No';

//-----------------------------------------------------------------

//Find largest id number
$query1 = "SELECT MAX(eventid) FROM subevents";
$result1 = mysqli_query($connection, $query1);
$row = mysqli_fetch_row($result1);

//Add 1 to id number for new entry
$evtnum1 = $row[0]+1 ;

//$evtnum1 = '20';
$evtnum2 = 'ev'.(string)($evtnum1);

// Add event to event database
// (eventid, name, date, location, address, free, website, tickets, description, hours, genre)
$query2 = "INSERT INTO subevents VALUES ('$evtnum1','$name', '$date1', '$date2','$location','$address','$district','$free','$website','$tickets','$description','$starttime','$genre','$email')";
$result2 = mysqli_query($connection, $query2);


if (!$result2) {
    echo "insert into database failed. " . mysqli_error($connection);
    exit();
} else {
    echo "Insert into database succesful.";
    header('Location: donate.php');
}
?>
