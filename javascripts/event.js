var latitude = $.cookie('latitudecookie');
var longitude = $.cookie('longitudecookie');

var startloc = {
    lat: parseFloat(latitude),
    lng: parseFloat(longitude)
};

function initMap() {
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var directionsService = new google.maps.DirectionsService;
    var map = baseMap();
    directionsDisplay.setMap(map);
    calculateAndDisplayRoute(directionsService, directionsDisplay);
    document.getElementById('mode').addEventListener('change', function() {
        calculateAndDisplayRoute(directionsService, directionsDisplay);
    });
}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
    var selectedMode = document.getElementById('mode').value;
    directionsService.route({
        origin: startloc,
        destination: endloc,
        travelMode: google.maps.TravelMode[selectedMode]
    }, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            var point = response.routes[0].legs[0];
            document.getElementById("travel_data").innerHTML =  'Travel: ' + point.duration.text ;
        } else {
            document.getElementById("travel_data").innerHTML = 'No directions found'
            baseMap();
        }
    });
}

function baseMap() {
    return new google.maps.Map(document.getElementById('map'), {
        zoom: 11,
        center: {
            lat: 43.653333,
            lng: -79.383889
        }
    });
}
