function getLocation() {
    var currentLocationType = $.cookie('locationtypecookie');
    if ((!currentLocationType || currentLocationType === 'ip' || currentLocationType === 'default') && navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    }
}

function showPosition(position) {
    var latitude = position.coords.latitude;
    var longitude = position.coords.longitude;

    $.cookie('locationtypecookie', 'geolocation');
    $.cookie('latitudecookie', latitude.toString());
    $.cookie('longitudecookie', longitude.toString());
    location.reload();
}

if ($.cookie('locationtypecookie')!="override"){
	getLocation();
}