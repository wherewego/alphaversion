<?php
session_start();
$pageTitle = 'Terms of Service';
include_once('utils.php');
include('header.php');
include('header2.php');
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-1 col-sm-2 col-md-3"></div>
        <div class="col-xs-10 col-sm-8 col-md-6" id="tos">
            <h2>Terms of Service</h2>
			<p>Last updated: February 9th 2016 
			</p>
            <p>These terms and conditions contain legal obligations. Please read these terms and conditions carefully before using the Wherewego Inc. (“Wherewego”) site. 
			</p>
			
			<h3>Introduction</h3>
			<p>Welcome to the Internet site of Wherewego. These Terms and Conditions apply to the Wherewego site. By using the Wherewego site, or any of the products or services (collectively the "services") available on the Wherewego site, you agree, without limitation or qualification, to be bound by these terms and conditions and such other additional or alternative terms, conditions, rules and policies which are displayed. If you do not agree with these Terms and Conditions, you may not use the Wherewego site or any of the services on it.
			</p>
			
			<p>Please note that we reserve the right, at our discretion, to change, modify, add, or remove portions of these Terms and Conditions at any time. Please check periodically for changes. Your continued use of the Wherewego site following the posting of any changes to these Terms and Conditions will mean you accept those changes.
			</p>
			
            <h3>Privacy</h3>
            <p>For information on how user information is collected, used, and disclosed by Wherewego in connection with your use of the Wherewego site and any of the services, please consult our privacy policy.
			</p>

            <h3>Content</h3>
            <p>Our service allows you to post, link, store, share, and otherwise make available certain information, text, graphics, videos, or other material. 
			</p>
			
			<p>By uploading materials or otherwise submitting any materials to us, you automatically grant (or warrant that the owner of such materials expressly grants) Wherewego a world-wide, perpetual, royalty-free, irrevocable and nonexclusive right and license to use, copy, adapt, transmit, communicate, publicly display and perform, distribute and create compilations and derivative works from such submitted materials, for the limited purposes of publishing and promoting such materials in connection with the service through which the materials were submitted or generated, and for all promotions thereof. Such license shall apply with respect to any form, media, or technology now known or later developed. In addition, you warrant that all "moral rights" in such materials have been waived.
			</p>
			
			<p>Please note that Wherewego reserves the right to refuse to post or to remove any information or materials, in whole or in part, that, in its sole discretion, are unacceptable, undesirable, or in violation of these Terms and Conditions.
			</p>

            <h3>Links</h3>
            <p>Our service may contain links to third¬ party web sites or services that are not owned or controlled by Wherwego. Wherewego has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party web sites or services. You further acknowledge and agree that Wherewego shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods, or services available on or through any such web sites or services. 
			</p>

            <h3>Changes</h3>
            <p>We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If a revision is material we will try to provide at least 30 days' notice prior to any new terms taking effect. What constitutes a material change will be determined at our sole discretion. 
			</p>

            <p>If you have any questions about these Terms, please contact us. 
			</p>

        </div>
        <div class="col-xs-1 col-sm-2 col-md-3"></div>
    </div>
</div>
<?php include('footer.php'); ?>