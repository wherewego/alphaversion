<?php
session_start();
$pageTitle = 'Privacy Policy';
include_once('utils.php');
include('header.php');
include('header2.php');
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-1 col-sm-2 col-md-3"></div>
        <div class="col-xs-10 col-sm-8 col-md-6" id="pripol">
            <h2>Privacy Policy</h2>
            <p>Last updated: February 9th 2016 
			</p>
			<p>Wherewego operates http://www.where-wego.com. This page informs you of our policies regarding the collection, use, and disclosure of personal information we receive from users of the site. Wherewego respects the privacy of all of its users and is committed to protecting such privacy in accordance with this Privacy Policy. We use your personal information only for providing and improving the site. By using the site, you agree to the collection and use of information in accordance with this policy. 
			</p>
            
			<h3>Information Collection</h3>
            <p>In the course of operating Wherewego, we may collect both personal and non-personal information.
			</p>
			<p>“Personal information” is personally identifiable information such as your name, address, email address, birth date, and gender. We collect personal information when you choose to provide it to us when using the Wherewego site. In addition, if you choose to contact any of our customer service representatives, we may keep a record of that correspondence so that we may fully address the subject matter.  
			</p>
			<p>Non-personal information is information of an anonymous nature, such as an Internet Protocol Address (IP Address), the domain used to access the Wherewego site, and the version of the browser or operating system being used. In addition, we may use third party services such as Google Analytics that collect, monitor, and analyze this information. 
			</p>
			
            <h3>Disclosure of Personal Information</h3>
            <p>We may use your Personal Information to contact you with newsletters, marketing, or promotional materials and other information that helps you know about upcoming events and deals in your area.
			</p>
			
			<p>We will not sell or rent your personal information to anyone. We will only disclose your personal information to third parties:
			</p>
			
			<p>		- Where you have specifically given us consent to disclose your personal information for a designated purpose;</p>
            <p>		- Who are acting on our behalf as our agents, suppliers, or service providers, solely to enable us to more efficiently provide you with Wherewego services. Such entities act under strict contractual controls, requiring them to maintain the confidentiality of all such personal information and to use such information solely for purposes related to the provision of the Wherewego services;</p>
			<p>		- As may be set out in the Terms and Conditions applicable to Wherewego, to facilitate the provision of such Wherewego service, such as fulfilling legal requirements;</p>
			<p>		- As required by law, including by an order of any court, institution, or body with jurisdiction or authority to compel the production of information, or in order to protect or defend a legal interest; or</p>
			<p>		- In connection with a Wherewego corporate re-organization, merger or amalgamation with another entity, or a sale of all or a substantial portion of the assets of Wherewego provided that the personal information disclosed continues to be used for the purposes permitted by this Privacy Policy by the entity acquiring the information.</p>
			
			<p>Cookies are files with small amounts of data, which may include an anonymous unique identifier. Cookies are sent to your browser from a web site and stored on your computer's hard drive. Like many sites, we use "cookies" to collect information. You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent. However, if you do not accept cookies, you may not be able to use some portions of our site. </p>
			
			<h3>Security</h3>
            <p>The security of your Personal Information is important to us, but remember that no method of transmission over the Internet, or method of electronic storage, is 100% secure. While we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security. </p>
            <p>You can help protect the security of your Personal Information as well. For instance, never give out your password, since this is what is used to access your Account or Registration Information. Also remember to log out of your account and close your browser window when you finish using a Wherewego service or surfing the web, so that other people using the same computer won't have access to your information. </p>
			
			<h3>Changes</h3>
            <p>This Privacy Policy is effective as of February 9st 2016 and will remain in effect except with respect to any changes in its provisions in the future, which will be in effect immediately after being posted on this page. We reserve the right to update or change our Privacy Policy at any time and you should check this Privacy Policy periodically. Your continued use of the service after we post any modifications to the Privacy Policy on this page will constitute your acknowledgment of the modifications and your consent to abide and be bound by the modified Privacy Policy. If we make any material changes to this Privacy Policy, we will notify you either through the email address you have provided us, or by placing a prominent notice on our website. </p>
            <h3>Contact Us</h3>
            <p>If you have any questions about this Privacy Policy, please contact us. </a></p>
        </div>
        <div class="col-xs-1 col-sm-2 col-md-3"></div>
    </div>
</div>
<?php include('footer.php'); ?>