<?php
//error_reporting(E_ALL); ini_set('display_errors', 'On'); 
//session_start();
include_once('db.php');

if ($_SESSION["event"] == "new") {    

    //Pull a random new event
    $evtid = ranevent($_SESSION["s_userid"],$connection);
    $_SESSION["evt_id"] = $evtid ;

} else {

    //Get Previous event number
    $prevnt = $_SESSION["evt_id"];

    //Find new non-seen event
    $evtid = recevent($_SESSION["s_userid"],$prevnt,$choice,$connection);
    $_SESSION["evt_id"] = $evtid ;

}

//find events within date selection criteria
function dateselect($connection,$criteria,$district) {

    // today: date_1<=today and date_2>=today
    // tomorrow: date_1<=tomorrow and date_2>=tomorrow
    // this week:  date_1<=d+7 and date_2>=d

    $today = date("Y-m-d");
    $tomorrow = date("Y-m-d",mktime(0, 0, 0, date("m")  , date("d")+1, date("Y")));
    $dplusseven = date("Y-m-d",mktime(0, 0, 0, date("m")  , date("d")+7, date("Y")));
    //$date = date("Y-m-d",mktime(0, 0, 0, date("m")  , date("d")+14, date("Y")));

    if ($criteria == 'Today') {
        $query4 = "SELECT eventid, district FROM evtable WHERE date_1<='". $today . "' AND date_2>='". $today ."'";
    } elseif ($criteria == 'Tomorrow') {
        $query4 = "SELECT eventid, district FROM evtable WHERE date_1<='". $tomorrow . "' AND date_2>='". $tomorrow ."'";
    } else {
        $query4 = "SELECT eventid, district FROM evtable WHERE date_1<='". $dplusseven . "' AND date_2>='". $today ."'";
    }
    //Change this:
    $result4 = mysqli_query($connection, $query4);

    $cnt=1;
    $datearray=[] ;

    //Find events within certain dates
    while($row = mysqli_fetch_row($result4)) {
        $datearray[$cnt] = $row[0] ;
        $districtarray[$cnt] = $row[1] ;
        $cnt=$cnt+1;
    }
    mysqli_free_result($result4);
    return array($datearray,$districtarray);
}

//pull a new random event (event=0) for new user-------------------------------------------

function ranevent($usid,$connection) {

    $criteria = $_SESSION["dates"] ; 
    $today = date("Y-m-d");
    $tomorrow = date("Y-m-d",mktime(0, 0, 0, date("m")  , date("d")+1, date("Y")));
    $dplusseven = date("Y-m-d",mktime(0, 0, 0, date("m")  , date("d")+7, date("Y")));
    //$date = date("Y-m-d",mktime(0, 0, 0, date("m")  , date("d")+14, date("Y")));

    if ($criteria == 'Today') {
        $randates = " evtable.date_1<='". $today . "' AND evtable.date_2>='". $today ."'";
    } elseif ($criteria == 'Tomorrow') {
        $randates = " evtable.date_1<='". $tomorrow . "' AND evtable.date_2>='". $tomorrow ."'";
    } else {
        $randates = " evtable.date_1<='". $dplusseven . "' AND evtable.date_2>='". $today ."'";
    }

    //Make sure random event is one that hasn't been seen yet
   $query1 = "SELECT evtable.eventid,evtable.date_1,user_events.userid,user_events.direction FROM evtable LEFT OUTER JOIN user_events ON evtable.eventid=user_events.eventid WHERE ( user_events.userid IS NULL OR user_events.userid<>".$usid.") AND (".$randates.") ORDER BY RAND() LIMIT 1" ; 

    $result1 = mysqli_query($connection, $query1);
    $row = mysqli_fetch_row($result1) ; 

    //generate random number from 1 to count
    $evtid = $row[0] ;
    if (strlen($evtid)==0){
        $evtid=0; 
    }

    return $evtid;
}

//------------------------------------------------------------------------------------------

function recevent($usid,$r,$choice,$connection) {

    $userdistrict = $_COOKIE['districtcookie'];

    if (isset($_COOKIE['districtcookie'])){
        $userdistrict = $_COOKIE['districtcookie'];
    } else {
        $userdistrict = 'A' ; 
    }


    $eventlimit=100;
    $userlimit =50;

    //Pick an event
    $selected = dateselect($connection,$_SESSION["dates"], $_COOKIE['districtcookie']);

    $datearray = $selected[0] ;
    $districtarray = $selected[1];

    $numevents = count($datearray);

    //Number of Events
    $maxev = $numevents ;

    //Number of users
    $maxus = $usid ;

    //current user: usid
    //last event: r

    //Fill matrix------------------------------------------------------------------------
    //Need random event selection
    $events = range(1,count($datearray));
    shuffle($events);

    $eventstring='(';

    for ($i = 1; $i <= count($datearray); $i++) {
        if ($i<$eventlimit) {
            $eventstring = $eventstring . $datearray[$events[$i-1]] .',';
        }
        if ($datearray[$events[$i-1]]==$r) {
            $eventstring = $eventstring . $datearray[$events[$i-1]] .',';
            if ($i<$eventlimit) {
                $lastev = $i-1 ;
            } else {
                $lastev = $eventlimit-1;
            }
        }

    }
    $eventtrim = rtrim($eventstring, ",");
    $eventtrim = $eventtrim.')' ; 

    // Bring in user data
    //Not selecting from all events?!?
    $query1 = "SELECT * FROM user_events WHERE eventid IN ".$eventtrim ; 
    $result1 = mysqli_query($connection, $query1);


    //From user_events create table
    $test = [] ;
    $a=0;

    while($row = mysqli_fetch_row($result1)) {
        $evid_q[$a] = $row[0] ; 
        $usid_q[$a] = $row[1] ; 
        $dirc_q[$a] = $row[2] ; 
        $a++;
    }

    $evid_uni = array_values(array_unique($evid_q)) ;
    $usid_uni = array_values(array_unique($usid_q)) ;

    //Create empty array
    $test = array_fill(0, count($usid_uni), array_fill(0, count($evid_uni), 0));

    for ($b=0; $b<count($usid_uni); $b++){ 
        for ($v=0; $v<count($evid_uni); $v++){         
            //$test[$b][$v] = 0 ;    
            for ($t=0; $t<count($evid_q); $t++){   
                if (($evid_q[$t]==$evid_uni[$v]) AND ($usid_q[$t]==$usid_uni[$b])){
                    $test[$b][$v] = $dirc_q[$t] ;   
                    break 2;          
                } 
            }     
        }
    } 
    
    // Find empty values ----------------------------------------------------------------
    $evtidary[1] = 0;
    $cnt=1;
    $row1 = $test[count($usid_uni)-1] ;

    #print_r($test) ; 
    #print_r($row1) ; 

    for ($j = 0; $j<count($row1)-1; $j++) {
        if ($row1[$j]==0) {
            $evtidary[$cnt]=$j+1;
            $cnt++ ;
        }
    }    

    if ($evtidary[1]==0) {
        //Choose random event
        return ranevent($_SESSION["s_userid"],$connection) ;
        exit();
    }

    $r = array_search($_SESSION["evt_id"],  $evid_uni) ; 

    //Compute similarity magnitude-----------------------------------------------------------------
    $evsize = sizeof($evtidary);
    $res = array_fill(1,$evsize,0);

    /*echo "</br >" ; 
    print_r($row1) ; 
    echo "</br >" ; 
    echo $_SESSION["evt_id"] ; 
    echo "</br >" ; 
    print_r($evid_uni) ; 
    echo "</br >" ;
    echo $r ; */

    for($i=1;$i<=$evsize-1;$i++) {

        $top=0;
        $mag1=1;
        $mag2=1;
        $g = $evtidary[$i]-1;
        $usrcnt = min(count($usid_uni)-1,$userlimit-1);

        for($p=0; $p<$usrcnt; $p++) {
            $x = $test[$p][$g] ;
            $y = $test[$p][$r] ;
            $top = $top + ($x * $y);
            $mag1 = $mag1 + ($x * $x);
            $mag2 = $mag2 + ($y * $y);
        }

        $evdistrict = $districtarray[$events[$evtidary[$i]-1]] ;
        $evname = $datearray[$events[$evtidary[$i]-1]] ;

        //Choose Multiplier depending on district
        if ($evdistrict==$userdistrict) {
            $multiplier=20 ;
        } else {
            switch ($userdistrict) {
                case "A":
                    if ($evdistrict=="D"||"B"||"E") {$multiplier=5;}
                    if ($evdistrict=="C") {$multiplier=2;}
                    if ($evdistrict=="F") {$multiplier=-1;}
                    break;
                case "B":
                    if ($evdistrict=="A"||"C") {$multiplier=5;}
                    if ($evdistrict=="D"||"E") {$multiplier=2;}
                    if ($evdistrict=="F") {$multiplier=-1;}
                    break;
                case "C":
                    if ($evdistrict=="B"||"A") {$multiplier=5;}
                    if ($evdistrict=="D"||"E") {$multiplier=2;}
                    if ($evdistrict=="F") {$multiplier=-1;}
                    break;
                case "D":
                    if ($evdistrict=="C"||"B"||"A") {$multiplier=5;}
                    if ($evdistrict=="E"||"F") {$multiplier=-1;}
                    break;
                case "E":
                    if ($evdistrict=="C"||"B"||"A") {$multiplier=5;}
                    if ($evdistrict=="D"||"F") {$multiplier=-1;}
                    break;
                case "F":
                    if ($evdistrict=="C"||"D"||"E"||"F") {$multiplier=5;}
                    if ($evdistrict=="B"||"A") {$multiplier=-1;}
                    break;
                default:
                   $multiplier=1;
            }
        }

        // Compute dot product across table events-------------------------------------------
        $res[$i] = $multiplier * $top / (sqrt($mag1)*sqrt($mag2));
    }

    //Alter for different choices
    if ($choice==1) {
        $maxs = array_keys($res, max($res));
        $num2 = $maxs[0] ;
    } else {
        $maxs = array_keys($res, min($res));
        $num2 = $maxs[0] ;
    }

    $evtid =  $datearray[$events[$evtidary[$num2]-1]]   ;
    mysqli_free_result($result1);
    return $evtid;
}
?>