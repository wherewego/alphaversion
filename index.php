<?php
//error_reporting(E_ALL); ini_set('display_errors', 'On'); 
session_start();

include_once('db.php');
include_once('utils.php');

if(isset($_GET['choice'])){
	$choice = $_GET['choice'] ;
}

if (isset($choice)) {
    if ($choice==1) {
        include('more.php');
    } else {
        include('less.php');
    }
} else {
    $choice=1; 
}

include('chevupdate.php');	

$query3 = " SELECT * FROM evtable WHERE eventid=$evtid LIMIT 1 ";
$result3 = mysqli_query($connection, $query3);
if (!$result3) {
    echo "Couldn't do query" .mysqli_error($connection);
}
$evname = mysqli_fetch_row($result3);
mysqli_free_result($result3);

setlocale(LC_ALL, 'en_US.UTF8');
function toAscii($str, $replace=array(), $delimiter='-') {
    if ( !empty($replace) ) {
        $str = str_replace((array)$replace, ' ', $str);
    }

    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

    return $clean;
}
$ntest = toAscii($evname[1]);
//ob_clean(); 
header('Location: event.php?id=' . $evtid . '&name=' . $ntest);
?>