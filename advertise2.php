<?php
session_start();
$pageTitle = 'About';
include_once('utils.php');
include('header.php');
include('header2.php');
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-1 col-sm-2 col-md-3"></div>
        <div class="col-xs-10 col-sm-8 col-md-6" id="advert">

            <h2>Advertising On Wherewego</h2>      
            
            <h3>Audience</h3>
            <p>By advertising with us you can reach a highly targeted audience specifically looking for events and event related information. You would also be helping to support a locally owned business. </p>

            <h3>Advertising Options</h3>
            <p>Wherewego offers both a promoted event option, placing your event in the normal usage of the site as well as banner advertising options. Sponsered tweets and facebook posts are also available.</p>

            <h3>Rates and Availability</h3>
            <p> For more information please contact us at <a href="mailto:contact@where-wego.com">contact@where-wego.com</a></p>

            <h3>Donation</h3>
            <p>If you feel so inclined you can also help support us through donation. Wherewego is Toronto owned and operated and through your support we would be able to bring you even more events you're waiting to find, as well as show your events to the widest possible audience. Thank you.   </p>

            <p><br /></p>

            <div id='abouth'>

                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                <input type="hidden" name="cmd" value="_s-xclick">
                <input type="hidden" name="encrypted" value="-----BEGIN PKCS7-----MIIHLwYJKoZIhvcNAQcEoIIHIDCCBxwCAQExggEwMIIBLAIBADCBlDCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb20CAQAwDQYJKoZIhvcNAQEBBQAEgYCMFK+Nnmtq4arMQEygwIDbU82EF25tizB56h6ENpccipfFK5GzyoTLD+vFAHw6tqt3XpLKa/Dk2gsDHMQCUpoF/AqLQfiJBc5jn8778IY6RYpepM5mGT3TwYfWcgSObTkJtYEjwBWJkHx8YgzOz8cO+ljKU0iFmdAzN57FdDXHLzELMAkGBSsOAwIaBQAwgawGCSqGSIb3DQEHATAUBggqhkiG9w0DBwQIAPyS20algeeAgYiEl7gQAp2KYD4xD08tWIO51PAJZMpwn2pbNYjkoBFyLVGi9aitywsfFar6cDllhunL4fgv57rEbkZ+SRR1S15NSnOn66HryeP3isV6hI7uSO4KLP1R7OVuQxHT+DK/X/3LfXZtkYZTXlxdPkLKyPXvESNq6Y88xJAZRiSh9n309jBHdc4uoFKgoIIDhzCCA4MwggLsoAMCAQICAQAwDQYJKoZIhvcNAQEFBQAwgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMB4XDTA0MDIxMzEwMTMxNVoXDTM1MDIxMzEwMTMxNVowgY4xCzAJBgNVBAYTAlVTMQswCQYDVQQIEwJDQTEWMBQGA1UEBxMNTW91bnRhaW4gVmlldzEUMBIGA1UEChMLUGF5UGFsIEluYy4xEzARBgNVBAsUCmxpdmVfY2VydHMxETAPBgNVBAMUCGxpdmVfYXBpMRwwGgYJKoZIhvcNAQkBFg1yZUBwYXlwYWwuY29tMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDBR07d/ETMS1ycjtkpkvjXZe9k+6CieLuLsPumsJ7QC1odNz3sJiCbs2wC0nLE0uLGaEtXynIgRqIddYCHx88pb5HTXv4SZeuv0Rqq4+axW9PLAAATU8w04qqjaSXgbGLP3NmohqM6bV9kZZwZLR/klDaQGo1u9uDb9lr4Yn+rBQIDAQABo4HuMIHrMB0GA1UdDgQWBBSWn3y7xm8XvVk/UtcKG+wQ1mSUazCBuwYDVR0jBIGzMIGwgBSWn3y7xm8XvVk/UtcKG+wQ1mSUa6GBlKSBkTCBjjELMAkGA1UEBhMCVVMxCzAJBgNVBAgTAkNBMRYwFAYDVQQHEw1Nb3VudGFpbiBWaWV3MRQwEgYDVQQKEwtQYXlQYWwgSW5jLjETMBEGA1UECxQKbGl2ZV9jZXJ0czERMA8GA1UEAxQIbGl2ZV9hcGkxHDAaBgkqhkiG9w0BCQEWDXJlQHBheXBhbC5jb22CAQAwDAYDVR0TBAUwAwEB/zANBgkqhkiG9w0BAQUFAAOBgQCBXzpWmoBa5e9fo6ujionW1hUhPkOBakTr3YCDjbYfvJEiv/2P+IobhOGJr85+XHhN0v4gUkEDI8r2/rNk1m0GA8HKddvTjyGw/XqXa+LSTlDYkqI8OwR8GEYj4efEtcRpRYBxV8KxAW93YDWzFGvruKnnLbDAF6VR5w/cCMn5hzGCAZowggGWAgEBMIGUMIGOMQswCQYDVQQGEwJVUzELMAkGA1UECBMCQ0ExFjAUBgNVBAcTDU1vdW50YWluIFZpZXcxFDASBgNVBAoTC1BheVBhbCBJbmMuMRMwEQYDVQQLFApsaXZlX2NlcnRzMREwDwYDVQQDFAhsaXZlX2FwaTEcMBoGCSqGSIb3DQEJARYNcmVAcGF5cGFsLmNvbQIBADAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTUxMTI1MTgzMzI2WjAjBgkqhkiG9w0BCQQxFgQULFOkHcSfPM42WWoF5As32klACbswDQYJKoZIhvcNAQEBBQAEgYBoV5MpuKfx01S6NnC+S3SFInKGGtA5Ry4ulE+W6hfL/W7ogwu39VMBaeUR8UicidUHfCg1BEsEBvg/spajza8ADi1FEeRNcio4gYaueiM4lW746Ehy4XWMgTRnZFNr7m7r62cpdSH9ZQDamp9/3S4lxsXEY1ViN6ShlIicMytWEg==-----END PKCS7-----
                ">
                <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                </form>

            </div>

        </div>
        <div class="col-xs-1 col-sm-2 col-md-3"></div>
    </div>
</div>
<?php include('footer.php');?>