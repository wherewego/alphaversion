<?php
//error_reporting(E_ALL); ini_set('display_errors', 'On'); 
//session_start();
function ensureEmptyStates() {
    // Check for empty cases
    if (!isset($_SESSION['dates'])) {
        $_SESSION['dates'] = 'Today';
    }

    if (!isset($_SESSION['event'])) {
        $_SESSION['event'] = 'new';
    }
    if (!isset($_SESSION["s_userid"])) {

        $connection= mysqli_connect("127.0.0.1","root","todo456", "eventtest2");    
        //$connection = mysqli_connect('localhost', 'root', '','wherewego');
        //$connection = mysqli_connect('wherewegocom.ipagemysql.com', 'silver_owl8', 'prata_coruja6','event_2');


        if (!$connection) {
            echo "Can't connect to database";
            echo "Cannot Connect" . mysqli_connect_errno() ."".mysqli_connect_error();
            exit();
        }

        $query1 = "SELECT MAX(userid) FROM ustable";
        $result1 = mysqli_query($connection, $query1);
        $usrow = mysqli_fetch_row($result1);

        //Add 1 to id number for new entry
        $newuserid = $usrow[0]+1 ;
        $_SESSION["s_userid"] = $newuserid ;

        //Insert new user into usertable
        $query1 = "INSERT INTO ustable (userid) VALUES ('$newuserid')";
        $result1 = mysqli_query($connection, $query1);
        if (!$result1) {
            echo "Couldn't do query" .mysqli_error($connection);
        }

    }
    if (!isset($_SESSION["evt_id"])) {
        $_SESSION["evt_id"] = 1;
    }
}
function geoLocationByIp() {
    $ipaddress = $_SERVER['REMOTE_ADDR'];

    // Grabs location IP address from IP address
    $url = 'http://ip-api.com/json/' . $ipaddress;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL,$url);
    $geoip_json = curl_exec($ch);
    curl_close($ch);

    // get the results and json_decode the answer
    $geoip = json_decode($geoip_json, true);
    return $geoip;
}

function getPostalCode($latitude, $longitude) {
    
	$url = 'http://api.geonames.org/findNearbyPostalCodesJSON?lat=' . $latitude . '&lng=' . $longitude . '&username=wherewego';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_URL,$url);
    $postal_code_json = curl_exec($ch);
    curl_close($ch);

    // get the results and json_decode the answer
    $postal_codes = json_decode($postal_code_json, true);
    $postal_code = $postal_codes['postalCodes'][0]['postalCode'];
    return $postal_code;
}

function districtLookup($postal_code) {
    $A = ['M5A', 'M7A', 'M5B', 'M5C', 'M5E', 'M5G', 'M5H', 'M5J', 'M6J', 'M5K', 'M6K',
         'M5L', 'M5S', 'M5T', 'M5V', 'M5W', 'M4X', 'M5X', 'M4Y'] ;

    $B = ['M3C', 'M6C', 'M6E', 'M4G', 'M6G', 'M4H', 'M6H', 'M4N', 'M5N', 'M4P', 'M5P',
         'M4R', 'M5R', 'M4S', 'M4T', 'M4V', 'M4W'] ;

    $C = ['M6A', 'M3B', 'M6B', 'M3H', 'M2K', 'M2L', 'M2M', 'M5M', 'M2N', 'M2P', 'M2R'] ;

    $D = ['M9A', 'M9B', 'M9C', 'M3J', 'M3K', 'M3L', 'M6L', 'M9L', 'M3M', 'M6M', 'M9M',
         'M3N', 'M6N', 'M9N', 'M6P', 'M9P', 'M6R', 'M9R', 'M6S', 'M8V', 'M9V', 'M8W',
         'M9W', 'M8X', 'M8Y', 'M8Z'] ;

    $E = ['M3A', 'M4A', 'M1B', 'M4B', 'M1C', 'M4C', 'M1E', 'M4E', 'M1G', 'M1H', 'M2H',
         'M1J', 'M2J', 'M4J', 'M1K', 'M4K', 'M1L', 'M4L', 'M1M', 'M4M', 'M1N', 'M1P',
         'M1R', 'M1S', 'M1T', 'M1V', 'M1W', 'M1X', 'M7Y'] ;

    if(in_array($postal_code, $A) > 0) {
        $district = 'A' ;
    } else if(in_array($postal_code, $B) > 0) {
        $district = 'B' ;
    } else if(in_array($postal_code, $C) > 0) {
        $district = 'C' ;
    } else if(in_array($postal_code, $D) > 0) {
        $district = 'D' ;
    } else if(in_array($postal_code, $E) > 0) {
        $district = 'E' ;
    } else{
        $district = 'A' ;
    }

    return $district;
}

function setDistrictName() {
    $district = $_COOKIE['districtcookie'];

    $names_by_district = array(
        'A' => 'Downtown',
        'B' => 'Midtown',
        'C' => 'North York',
        'D' => 'West Toronto',
        'E' => 'East Toronto'
    );

    if (array_key_exists($district, $names_by_district)) {
        $district_name = $names_by_district[$district];
    } else {
        $district_name = 'GTA';
    }

    $_SESSION['districtname'] = $district_name;
}

function setDefaultLocation() {
	
    if(!isset($_COOKIE['latitudecookie']) || !isset($_COOKIE['longitudecookie'])) {
        /* $geoLocation = geoLocationByIp();

        if ($geoLocation['success']) {
            $location_type = 'ip';
            $latitude = $geoLocation['lat'];
            $longitude = $geoLocation['lon'];
        } else {
            $location_type = 'default';
            $latitude = 43.645278;
            $longitude = -79.380556;
			setcookie('districtcookie', 'A');
        } */
		$location_type = 'default';
		$latitude = 43.645278;
		$longitude = -79.380556;
		setcookie('districtcookie', 'A');
        setcookie('locationtypecookie', $location_type);
        setcookie('latitudecookie', $latitude);
        setcookie('longitudecookie', $longitude);		
		header("Refresh:0");		
    }
	
	if(isset($_COOKIE['locationtypecookie'])){	
		if(($_COOKIE['locationtypecookie'] != 'default') || ($_COOKIE['locationtypecookie'] != 'override')) {
			$lat  = number_format ( $_COOKIE['latitudecookie'] ,4 ) ; 
			$long = number_format ( $_COOKIE['longitudecookie'] ,4 ) ; 		
			$postal_code = getPostalCode($lat, $long);
			$district = districtLookup($postal_code);
			setcookie('districtcookie', $district);				
		}			
		//if (!isset($_COOKIE['districtcookie'])||($_COOKIE['districtcookie']!=$district)){
		if (!isset($_COOKIE['districtcookie'])){
			header("Refresh:0");
		} else {
			setDistrictName();
		}	
	}
}
setDefaultLocation();
ensureEmptyStates(); 
?>