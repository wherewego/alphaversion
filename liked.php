<?php
session_start();
include_once('db.php');
include_once('utils.php');
$pageTitle = "Liked | Events";
include('header.php');
include('header2.php');

//Display liked events then all other events, search function?
$usplace = $_SESSION["s_userid"] ;

//Find user data
$query = " SELECT * FROM user_events WHERE userid=$usplace ";
$result = mysqli_query($connection, $query);
if (!$result) {
    echo "Couldn't do query" .mysqli_error($connection);
}


$a=0;
while($row = mysqli_fetch_row($result)) {
    $evid_q[$a] = $row[0] ; 
    $usid_q[$a] = $row[1] ; 
    $dirc_q[$a] = $row[2] ; 
    $a++;
}

//From user data select out liked events
$eventlist = array(array_fill(0,12,0));
$cnt=0;

if (isset($dirc_q)>0){
    foreach ($dirc_q as $key => $value) {
        if ($value==1) {
            $evselect = $evid_q[$key]; 
            $query2 = " SELECT * FROM evtable WHERE eventid=$evselect LIMIT 1";
            $result2 = mysqli_query($connection, $query2);
            $row2 = mysqli_fetch_row($result2);
            $eventlist[$cnt] = $row2 ;
            $cnt=$cnt+1 ;
        }    
    }
}

//Find all of today's events to list
$cnt2=0;
$alllist = array(array_fill(0,12,0));
$today = date("Y-m-d");

// Sort rest of today's events by number of likes
// Add new column to evtable with sum(direction) over user_events, list according to that column


#$query3 = " SELECT * FROM evtable WHERE date_1='".$today."' LIMIT 100";
$query3 = "Select * FROM evtable WHERE '".$today."'>=date_1 AND '".$today."'<=date_2 " ; 

$result3 = mysqli_query($connection, $query3);
while($row3 = mysqli_fetch_row($result3)) {
    if (sizeof($row3)>0) {
        $alllist[$cnt2] = $row3 ;
        $cnt2=$cnt2+1 ;
    }
}

setlocale(LC_ALL, 'en_US.UTF8');
function toAscii($str, $replace=array(), $delimiter='-') {
    if ( !empty($replace) ) {
        $str = str_replace((array)$replace, ' ', $str);
    }

    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = strtolower(trim($clean, '-'));
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
    return $clean;
}

//mysql_close($connection);

?>

<div id="list">

    <h3>Liked Events</h3>
    <?php

        if ($cnt==0) {
            echo "No Liked Events "."<br/>";
        } else {

            //print_r($eventlist)   ;

            for ($x = 0; $x < $cnt; $x++) {
                $ntest = toAscii($eventlist[$x][1]);
                echo $eventlist[$x][1] .' at '. $eventlist[$x][4]  ;

                echo "<div id='listlinks'>";
                echo "↪";
                echo " <a href=" . "'event.php?id=".$eventlist[$x][0]."&name=".$ntest."' ". "target='_blank' >Directions</a> ";
                echo " <a href=" . $eventlist[$x][8]. " target='_blank' >Website</a> ";
                echo " <a href=" . $eventlist[$x][9]. " target='_blank' >Tickets</a> " .  "<br/>";
                echo "</div>";
                echo "</br>";
            }
        }
    ?>

    <h3>All of Today's Events</h3>
    <?php

        if ($cnt2==0) {
            echo "No Events"."<br/>";
        } else {
            for ($x = 0; $x < $cnt2; $x++) {
                $ntest = toAscii($alllist[$x][1]);
                echo $alllist[$x][1] .' at '. $alllist[$x][4]  ;

                echo "<div id='listlinks'>";
                echo "↪";
                echo " <a href=" . "'event.php?id=".$alllist[$x][0]."&name=".$ntest."' ". "target='_blank' >Directions</a> ";
                echo " <a href=" . $alllist[$x][8]. " target='_blank' >Website</a> ";
                echo " <a href=" . $alllist[$x][9]. " target='_blank' >Tickets</a> " .  "<br/>";
                echo "</div>";
                echo "</br>";
            }
        }
    ?>
</div>
<?php include('footer.php'); ?>